import { DatePipe } from '@angular/common';
import { Component, ViewChild } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import * as moment from 'moment';
import { DataService } from './data-service.service';

export interface PeriodicElement {
  id: number;
  city: string;
  start_date: string;
  end_date: string;
  price: number;
  status: string;
  color: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'angular-assignment';
  displayedColumns: string[] = [
    'id',
    'city',
    'start_date',
    'end_date',
    'price',
    'status',
    'color',
  ];
  private data: PeriodicElement[] = [];
  dataSource = new MatTableDataSource(this.data);
  pipe: DatePipe;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  length = 1000;
  pageSize = 100;
  pageSizeOptions: number[] = [100, 200];
  pageEvent: PageEvent;

  setPageSizeOptions(setPageSizeOptionsInput: string) {
    if (setPageSizeOptionsInput) {
      this.pageSizeOptions = setPageSizeOptionsInput
        .split(',')
        .map((str) => +str);
    }
  }
  filterForm = new FormGroup({
    fromDate: new FormControl(),
    toDate: new FormControl(),
  });
  constructor(private dataService: DataService) {
    this.dataService.getData().subscribe((res) => {
      this.length = res.length;
      res.forEach((element) => {
        element.start_date = moment(element.start_date);
        element.end_date = moment(element.end_date);
      });
      this.dataSource = new MatTableDataSource(res);
      this.pipe = new DatePipe('en');
      this.dataSource.filterPredicate = (data, filter) => {
        if (this.fromDate && this.toDate) {
          return (
            data.start_date >= this.fromDate && data.start_date <= this.toDate
          );
        }
        return true;
      };
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter() {
    this.dataSource.filter = '' + Math.random();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  get fromDate() {
    return this.filterForm.get('fromDate').value;
  }
  get toDate() {
    return this.filterForm.get('toDate').value;
  }
}
