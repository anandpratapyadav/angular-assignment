# AngularAssignment

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.1.0.

## Development server

Clone this from repository. Install Dependancis by using command `npm install`.

Run `ng serve --prod` for a prod server. Navigate to `http://localhost:4200/` with Base API URL which is Mocky api.

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. with Base API URL which will be local server for api which is cloned from `https://gitlab.com/anandpratapyadav/django-assignment`. This the local set up for API. API is created in Python Djnago fraemwork.
